const user = require ('./../../scheme/users')
const log = require ('./../../libs/logs')(module);
const adres = "127.0.0.1";
module.exports= function (req, res){
        res.render('api',{mas:[
            {
                url:adres+"/mail",
                type:"POST",
                param:"{\n  subject\n  text\n  to\n}",
                result:"String",
                description:"Принимает текс, заголовок и адрес, после чего отправлет сообщение",
                singIn:"Авторизация не требуется",
            },
            {
                url:adres+"/singupadmin",
                type:"POST",
                param:"{\n  name\n  password\n}",
                result:"{\n  token\n  name\n  salt\n  hashPassword\n  _id\n}",
                description:"Регистрация администратора",
                singIn:"Авторизация не требуется",
            },
            {
                url:adres+"/singinadmin",
                type:"POST",
                param:"{\n  name\n  password\n}",
                result:"{\n  token\n}",
                description:"Авторизация администратора",
                singIn:"Авторизация не требуется",
            },
            {
                url:adres+"/search",
                type:"POST",
                param:"{\n  ip\n}",
                result:"[\n  {\n    date\n    num\n    _id\n    ip\n  }\n]",
                description:"Производит поиск в базе ip-адресов",
                singIn:"Авторизация требуется",
            },
            {
                url:adres+"/saveFile",
                type:"POST",
                param:"{\n  file\n}",
                result:"String",
                description:"Сохранит файл, не меня расширение, в папку files",
                singIn:"Авторизация не требуется",
            },
            {
                url:adres+"/edit",
                type:"POST",
                param:"{\n  id\n  num\n}",
                result:"String",
                description:"Изменяет значение поля num у записи с прокинутым id",
                singIn:"Авторизация требуется",
            },
        ]});
}