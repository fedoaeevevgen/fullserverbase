const adminChek = require('./../libs/adminChek')

module.exports = async function(app){  
    app.get('/api',require('./api'))
    app.post('/singUpAdmin', require('./singUpAdmin'));
    app.post('/singInAdmin', require('./singInAdmin'));
    app.post('/mail', require('./mail'));
    app.post('/saveFile', require('./saveFile'));
    app.post('/edit',adminChek, require('./edit'));
    app.post('/search',adminChek, require('./search'));
};