const saveFile = require ('./../../libs/saveFiles')
const log = require ('./../../libs/logs')(module);
module.exports=function (req, res){
    saveFile('/',req.files[0],'name',function (err,file) {
        if(err){
            log.error(err);
            res.send('С фото произошла ошибка');
        }else{
            res.send('Фото успешно сохранено');
        }
    })
}