const jwt = require('jsonwebtoken');
const config = require('./config');
const log = require('./logs');
module.exports = function verify(req, res, next){
    try{
        jwt.verify(req.cookies.token, config.jwt.secretOrKey, function (err, decoded) {
            if (decoded !== undefined){
                next()
            } else  if (err) {
                res.send('Вы не авторизированны');
            }
        });
    } catch (err) {
        if (err.message === "Cannot read property 'refreshToken' of null"){
            res.send('Вы не авторизированны');
        } else {
            log.error(err)
            res.send(err.message);
        }
    }
}
