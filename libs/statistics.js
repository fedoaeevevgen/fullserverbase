const users = require('../scheme/users')
const log = require('./logs')(module);
module.exports = function static(req,res,next) {
    users.findOne({ip:req.connection.remoteAddress}).exec((err, result) => {
        if (err) {
            log.error(err)
            next()
        }else
            if(result===null){
                const newUser = new users({
                    ip:req.connection.remoteAddress
                })
                newUser.save()
                next()
            }else{
                result.set({num:result.num+1})
                result.save()
                next();
            }
      });
}