const mongoose = require('mongoose');
const usersSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date:{
        type:Date,
        default:new Date()
    },
    ip:{
        type:String,
        required:true
    },
    num:{
        type:Number,
        default:1
    },
},{
    collection: "users",
    versionKey: false
});

usersSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('users', usersSchema);